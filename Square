    /// <summary>
        /// Function that calculate square of angled triangle
        /// </summary>
        /// <param name="lengths">Lengths of sides</param>
        /// <returns>Square</returns>
        private static double Square(params double[] lengths)
        {
            if (lengths.Length != 3)
            {
                throw new Exception("Shape isn't triangle!");
            }
            foreach (var length in lengths)
            {
                if (length <= 0)
                {
                    throw new Exception("Triangle can be only positive sides!");
                }
            }
            Array.Sort(lengths);
            if (lengths[0]*lengths[0] + lengths[1]*lengths[1] != lengths[2]*lengths[2])
            {
                throw new Exception("This triangle isn't angled!");
            }
            return (0.5*lengths[0]*lengths[1]);
        }