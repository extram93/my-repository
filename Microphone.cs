using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using NAudio.Wave;

namespace Sound_Ping
{
    class Microphone
    {
        WaveIn _microphone;

        public BinaryWriter Bw;
        //
        public bool SaveToFile = false;
        readonly MainWindow _mainWindow;
        int _numberOfSample;
        int _firstLevel =  40;
        int _secondLevel = 40;
        //
        int _firstLevelMax;
        int _secondLevelMax;
        //
        //
        public int Interval = 20;
        List<double> Times = new List<double>();

        const uint MnHysteresisMaximum = 300 /* ms */ * 48000 /* sample rate */ / 1000 /* ms into sec */;
        uint _mNFirstChannelCounter;
        uint _mNSecondChannelCounter;
        int _mNFirstChannelEdge;
        int _mNSecondChannelEdge;

        //
        public Microphone(MainWindow window)
        {
            if (File.Exists("output"))
            {
                File.Delete("output");
            }

            Bw = new BinaryWriter(File.OpenWrite("output"));
            _mainWindow = window;
            StartDefaultParams();
        }
        //

        //
        public void ChangeLevelParams(int firstLevel, int secondLevel)
        {
            _firstLevel = firstLevel;
            _secondLevel = secondLevel;
        }
        public void ReloadMicrophoneParams(int deviceId)
        {
            _microphone.StopRecording();
            _microphone.Dispose();
            Initialize(deviceId);
            _microphone.StartRecording();
        }
        public void StartDefaultParams()
        {
            Initialize(0);
            _microphone.StartRecording();
        }
        void Initialize(int deviceId)
        {
            _microphone = new WaveIn();
            _microphone.DeviceNumber = deviceId;
            _microphone.BufferMilliseconds = 10;
            _microphone.WaveFormat = new WaveFormat(48000, 8, 2);
            _microphone.DataAvailable +=Microphone_DataAvailable;
        }
        void CheckLevels(List<byte> first, List<byte> second, int number)
        {
                if (_firstLevelMax<first.Max())
                {
                    _firstLevelMax = first.Max();
                }
                if (_secondLevelMax < second.Max())
                {
                    _secondLevelMax = second.Max();
                }
                if (number % Interval == 0)
                {
                    _mainWindow.pbFirstMic.Value = _firstLevelMax;
                    _mainWindow.pbSecondMic.Value = _secondLevelMax;
                    _firstLevelMax = _secondLevelMax = 0;
                }
        }

        void ProcessDelta()
        {
          int nDelta = _mNFirstChannelEdge - _mNSecondChannelEdge;
          if (Math.Abs(nDelta) > 24000 /* TODO 500 ms hardcode need remove */)
          {
            return;
          }

          double dblDeltaInMs = nDelta / 48.0;
          // Need to Show new Value
          Times.Add(Math.Abs(dblDeltaInMs));
          if (Times.Count > 3)
          {
              Times.RemoveAt(0);
          }
          WriteToForm(Times);

        }

        void Microphone_DataAvailable(object sender, WaveInEventArgs e)
        {
            List<byte> firstChannel = new List<byte>();
            List<byte> secondChannel = new List<byte>();
            //
            for (int i = 0; i < e.Buffer.Length - 1; i += 2)
            {
              byte byFirstSample = (byte) (Math.Abs(e.Buffer[i] - 127));
              byte bySecondSample = (byte) (Math.Abs(e.Buffer[i + 1] - 127));
              int nSampleIndex = i / 2 + _numberOfSample * 480;

              firstChannel.Add(byFirstSample);
              secondChannel.Add(bySecondSample);

              // Processing of sample
              // First
              if (_mNFirstChannelCounter > 0)
              {
                --_mNFirstChannelCounter;
              }

              if (byFirstSample > _firstLevel)
              {
                if (_mNFirstChannelCounter == 0)
                {
                  // Now the step from silent to sound
                  _mNFirstChannelEdge = nSampleIndex;
                  ProcessDelta();
                }

                _mNFirstChannelCounter = MnHysteresisMaximum;
              }

              // Second
              if (_mNSecondChannelCounter > 0)
              {
                --_mNSecondChannelCounter;
              }

              if (bySecondSample > _secondLevel)
              {
                if (_mNSecondChannelCounter == 0)
                {
                  // Now the step from silent to sound
                  _mNSecondChannelEdge = nSampleIndex;
                  ProcessDelta();
                }
                
                _mNSecondChannelCounter = MnHysteresisMaximum;
              }
              if (SaveToFile)
              {
                  Bw.Write((short)(256 * (e.Buffer[i] - 127)));
                  Bw.Write((short)(256 * byFirstSample));
                  Bw.Write((short)_mNFirstChannelCounter);
                  Bw.Write((short)(256 * (e.Buffer[i + 1] - 127)));
                  Bw.Write((short)(256 * bySecondSample));
                  Bw.Write((short)_mNSecondChannelCounter);
              }

            }
            CheckLevels(firstChannel, secondChannel, _numberOfSample);

            _numberOfSample++;
        }

        void WriteToForm(List<double> times)
        {
            if (times.Count == 1)
            {
                _mainWindow.tbFirstTime.Text = string.Format("{0:F1}", times[0]);
            }
            if (times.Count == 2)
            {
                _mainWindow.tbFirstTime.Text = string.Format("{0:F1}", times[1]);
                _mainWindow.tbSecondTime.Text = string.Format("{0:F1}", times[0]);
            }
            if (times.Count == 3)
            {
                _mainWindow.tbFirstTime.Text = string.Format("{0:F1}", times[2]);
                _mainWindow.tbSecondTime.Text = string.Format("{0:F1}", times[1]);
                _mainWindow.tbThirdTime.Text = string.Format("{0:F1}", times[0]);
            }
            _mainWindow.tbAverage.Text = string.Format("{0:F1}", times.Average());
        }
    }
}
